from datetime import datetime
from hashlib import sha1
import json
import os
from urllib.parse import urlparse, urlencode

from logger import logger

import boto3
import youtube_dl

s3 = boto3.client("s3")


def dict2tag(d):
    return {"TagSet": [{"Key": str(k), "Value": str(v)} for k, v in d.items()]}


def lambda_handler(event, context):
    now = datetime.utcnow()
    url = event["queryStringParameters"]["url"]
    audio = event["queryStringParameters"].get("audio", False)

    url_parts = urlparse(url)
    url_hash = sha1(url.encode()).hexdigest()
    hostname_hash = sha1(url_parts.hostname.encode()).hexdigest()

    # yt-dl stuff
    destination_directory = "/tmp"
    destination_file_name = url_hash

    destination_file_path = os.path.join(destination_directory, destination_file_name)

    options = json.loads(os.environ["YOUTUBEDL_OPTIONS"])
    options.update(
        {
            "outtmpl": destination_file_path,
            "cachedir": False,
            "ffmpeg_location": "/opt/bin/ffmpeg",
        }
    )

    # s3 stuff
    key_path = "{hostname_hash}/{year}/{month:02d}/{day:02d}/{dt}-{url_hash}".format(
        hostname_hash=hostname_hash,
        year=now.year,
        month=now.month,
        day=now.day,
        dt=now.isoformat(),
        url_hash=url_hash,
    )

    info_key_file_path = "{key_path}.info.json".format(key_path=key_path)

    # configure additioanl audio options
    if audio:
        options["format"] = "bestaudio"
        options["postprocessors"] = [
            {
                "key": "FFmpegExtractAudio",
                "preferredcodec": "mp3",
                "preferredquality": "5",
                "nopostoverwrites": False,
            }
        ]
        content_type = "audio/mpeg"
        download_key_file_path = "{key_path}.audio.mp3".format(key_path=key_path)
        dl_file_path = destination_file_path
    else:
        content_type = "video/webm"
        download_key_file_path = "{key_path}.video.mkv".format(key_path=key_path)
        dl_file_path = destination_file_path + ".mkv"

    logger.info(options)

    with youtube_dl.YoutubeDL(options) as ydl:
        # download and write any video info to s3

        # We just want to extract the info at first
        info = ydl.extract_info(url, download=False)

        # upload info
        save_state = s3.put_object(
            Body=json.dumps(info),
            Bucket=os.environ["STORAGE_BUCKET"],
            Key=info_key_file_path,
            ContentType="application/json",
            Tagging=dict2tag(
                {
                    "url": url,
                    "hostname": url_parts.hostname,
                    "downloaded": now.isoformat(),
                }
            ),
        )

        # download and write video to s3
        status = ydl.download([url])
        if not event.get("test"):
            source_file = open(dl_file_path, "rb")
            save_state = s3.put_object(
                Body=source_file,
                Bucket=os.environ["STORAGE_BUCKET"],
                Key=download_key_file_path,
                ContentType=content_type,
                Tagging=dict2tag(
                    {
                        "url": url,
                        "hostname": url_parts.hostname,
                        "downloaded": now.isoformat(),
                    }
                ),
            )

    logger.info(info)

    return {
        "statusCode": "201",
        "body": json.dumps(
            {
                "bucket_url": "https://{storage_bucket}.s3.amazonaws.com".format(
                    storage_bucket=os.environ["STORAGE_BUCKET"]
                ),
                "info_key": info_key_file_path,
                "download_key": download_key_file_path,
            }
        ),
        "headers": {"Content-Type": "application/json"},
    }
